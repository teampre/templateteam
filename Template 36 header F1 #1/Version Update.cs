﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1 {
    class Version_Update {
        //V1.01.02
        //Edit bug event form load add clear file login begin gen file login to exe updata

        //V1.01.03
        //Edit call define_class from in LoadTestSpec to setup
        //Add dryice_scan2d_get to main function class
        //Add config show cmd in main running
        //Edit function relay matric to public
        //Edit bit relay matric

        //V1.01.04
        //Add function limit current dmm
        //Edit name function dmm

        //V1.05
        //Add close main Application.ExitThread(); , Environment.Exit(0);
        //Add class Server and Client

        //V2022.06
        //Add prism.mode in class
        //Add clear rtfTerminal if TextLength == 50000
        //Add Clear file camera before open
        //Add Function OscConnect()
        //Edit bug open form frist user id show old

        //V2022.07
        //Edit bug stamp sn old to prism if read2d fail

        //V2022.08
        //Edit ReTest if [sn.pass] show fail to show pass
        //Add flagNotReTest
        //Add popup yes no retest

        //V2022.09
        //Add Eval check head function to upper and lower
        //Edit color label result pass fail to color.green

        //V2022.10
        //Edit size changed can adjust size all

        //V2023.01
        //Show gui when ready

        //V2023.02
        //Add put unit alternate head with program ScanSN

        //V2023.03
        //Edit bug ยกงานออกแล้วไม่เคลีย SN ในโปรแกรม Scan2D
        //Edit Code get pin head [ old == ] [ New Contains ] บางทีมันตอบกลับมา 65535\r\n0\r\n มันควรจะเป็น 0\r\n

        //V2023.04
        //Add function Discom

        //V2023.05
        //เพิ่มการ disPort arduino ด้วยถ้า reset ไป 2 ครั้งแล้วไม่หาย
        //แก้ฟังก์ชั่นเขียนไฟล์ camera_list นิดนึง ใส่ try ไว้ด้วย

        //V2023.06
        //เพิ่มคำสั่ง <[< เอาไว้กำหนด path ของ result.txt เอง
        //เวอร์ชั่นนี้สามารถใช้ร่วมกับโปรแกรมเล็กที่เปิดตลอดเวลาได้แล้ว

        //V2023.07
        //add ปรับความยาว column dataGridView ได้

        //V2023.08
        //add UI_config
    }
}
