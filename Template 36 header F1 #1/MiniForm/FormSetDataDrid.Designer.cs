﻿namespace WindowsFormsApplication1.MiniForm {
    partial class FormSetDataDrid {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_step = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_spec = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_measure = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tb_result = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.hsb_step = new System.Windows.Forms.HScrollBar();
            this.hsb_spec = new System.Windows.Forms.HScrollBar();
            this.hsb_measure = new System.Windows.Forms.HScrollBar();
            this.hsb_result = new System.Windows.Forms.HScrollBar();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.hsb_step);
            this.groupBox1.Controls.Add(this.tb_step);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 60);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // tb_step
            // 
            this.tb_step.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_step.Location = new System.Drawing.Point(94, 19);
            this.tb_step.Name = "tb_step";
            this.tb_step.Size = new System.Drawing.Size(96, 29);
            this.tb_step.TabIndex = 1;
            this.tb_step.Text = "0";
            this.tb_step.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_step.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_step_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "STEP";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.hsb_spec);
            this.groupBox2.Controls.Add(this.tb_spec);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 62);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // tb_spec
            // 
            this.tb_spec.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_spec.Location = new System.Drawing.Point(94, 19);
            this.tb_spec.Name = "tb_spec";
            this.tb_spec.Size = new System.Drawing.Size(96, 29);
            this.tb_spec.TabIndex = 1;
            this.tb_spec.Text = "0";
            this.tb_spec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_spec.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_spec_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "SPEC";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.hsb_measure);
            this.groupBox3.Controls.Add(this.tb_measure);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(12, 146);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(258, 60);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // tb_measure
            // 
            this.tb_measure.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_measure.Location = new System.Drawing.Point(94, 19);
            this.tb_measure.Name = "tb_measure";
            this.tb_measure.Size = new System.Drawing.Size(96, 29);
            this.tb_measure.TabIndex = 1;
            this.tb_measure.Text = "0";
            this.tb_measure.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_measure.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_measure_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "MEASURE";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.hsb_result);
            this.groupBox4.Controls.Add(this.tb_result);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(12, 212);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(258, 62);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            // 
            // tb_result
            // 
            this.tb_result.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_result.Location = new System.Drawing.Point(94, 19);
            this.tb_result.Name = "tb_result";
            this.tb_result.Size = new System.Drawing.Size(96, 29);
            this.tb_result.TabIndex = 1;
            this.tb_result.Text = "0";
            this.tb_result.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_result.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_result_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "RESULT";
            // 
            // hsb_step
            // 
            this.hsb_step.Location = new System.Drawing.Point(205, 19);
            this.hsb_step.Maximum = 10000;
            this.hsb_step.Name = "hsb_step";
            this.hsb_step.Size = new System.Drawing.Size(45, 29);
            this.hsb_step.TabIndex = 5;
            this.hsb_step.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsb_step_Scroll);
            // 
            // hsb_spec
            // 
            this.hsb_spec.Location = new System.Drawing.Point(205, 19);
            this.hsb_spec.Maximum = 10000;
            this.hsb_spec.Name = "hsb_spec";
            this.hsb_spec.Size = new System.Drawing.Size(45, 29);
            this.hsb_spec.TabIndex = 6;
            this.hsb_spec.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsb_spec_Scroll);
            // 
            // hsb_measure
            // 
            this.hsb_measure.Location = new System.Drawing.Point(205, 19);
            this.hsb_measure.Maximum = 10000;
            this.hsb_measure.Name = "hsb_measure";
            this.hsb_measure.Size = new System.Drawing.Size(45, 29);
            this.hsb_measure.TabIndex = 7;
            this.hsb_measure.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsb_measure_Scroll);
            // 
            // hsb_result
            // 
            this.hsb_result.Location = new System.Drawing.Point(205, 19);
            this.hsb_result.Maximum = 10000;
            this.hsb_result.Name = "hsb_result";
            this.hsb_result.Size = new System.Drawing.Size(45, 29);
            this.hsb_result.TabIndex = 8;
            this.hsb_result.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsb_result_Scroll);
            // 
            // FormSetDataDrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(280, 282);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormSetDataDrid";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSetDataDrid";
            this.Load += new System.EventHandler(this.FormSetDataDrid_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_step;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tb_spec;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tb_measure;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tb_result;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.HScrollBar hsb_step;
        private System.Windows.Forms.HScrollBar hsb_spec;
        private System.Windows.Forms.HScrollBar hsb_measure;
        private System.Windows.Forms.HScrollBar hsb_result;
    }
}