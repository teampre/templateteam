﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using USBClassLibrary;
using Spire.Xls;
using System.Drawing;
using System.Reflection;
using System.Diagnostics;
using System.IO.Ports;
using System.Management;
using System.Text;
using System.Linq;

namespace WindowsFormsApplication1 {
    public class PSU {
        public string nameDMM { get; set; }
        public string nameOSC { get; set; }
        public Define define { get; set; }
        public MessageErr messageErr { get; set; }
        public DMM dmm { get; set; }
        public OSC osc { get; set; }


        public PSU() {
            define = new Define();
            messageErr = new MessageErr();
            dmm = new DMM();
            osc = new OSC();
        }

        public class DMM {
            public string connected { get; set; }
            public string clearErr { get; set; }
            public string readResister { get; set; }
            public string readVoltage { get; set; }
            public string readCurrent { get; set; }
            public string setLocal { get; set; }
            public string selectHold { get; set; }
            public string onHold { get; set; }
            public string offHold { get; set; }
            public string onBeeper { get; set; }
            public string offBeeper { get; set; }
            public string sourceOn { get; set; }
            public string sourceOff { get; set; }
            public string volt { get; set; }

            public DMM() {
                connected = "Connected";
                clearErr = "*CLS\n";
                readResister = ":MEAS:RES? ";
                readVoltage = ":MEAS:VOLT:DC? ";
                readCurrent = "MEAS:CURR:DC? ";
                setLocal = ":SYST:LOC\n";
                selectHold = "CALC:FUNC HOLD";
                onHold = "CALC:STAT ON";
                offHold = "CALC:STAT OFF";
                onBeeper = "SYSTem:BEEPer:STATe 1";
                offBeeper = "SYSTem:BEEPer:STATe 0";
                sourceOn = "OUTP ON";
                sourceOff = "OUTP OFF";
                volt = "VOLT ";
            }
        }
        public class OSC {
            public string readFetc { get; set; }
            public string clearErr { get; set; }
            public string read { get; set; }

            public OSC() {
                readFetc = "INIT;FETC?";
                clearErr = "CLS\n";
                read = ":READ?";
            }
        }
        public class Define {
            public string nameOSC { get; set; }
            public string connect { get; set; }
            public string OSC { get; set; }
            public string DMM { get; set; }
            public string deviceGet { get; set; }
            public string deviceDescription { get; set; }
            public string deviceName { get; set; }
            public string TF960 { get; set; }
            public string pidOSC { get; set; }
            public string vidOSC { get; set; }


            public Define() {
                nameOSC = "0x0957::0x1807";
                connect = "Connected";
                OSC = "Oscilloscope";
                DMM = "DMM";
                deviceGet = "SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'";
                TF960 = "TTi TF960";
                deviceDescription = "Description";
                deviceName = "NAME";
                pidOSC = "0492";
                vidOSC = "103E";
            }
        }
        public class MessageErr {
            public string connectDMM { get; set; }
            public string fineNameDMM { get; set; }
            public string fineNameOSC { get; set; }

            public MessageErr() {
                connectDMM = "\nDMM no connect";
                fineNameDMM = "\nCan't find DMM name";
                fineNameOSC = "\nCan't find OSC name";
            }
        }
    }
    public class SetFile {
        public void cameraList() {
            //เป็นไฟล์ที่กำหนดว่ากล้องตัวไหนจะเปิดก่อนเปิดหลัง ให้มันเรียงตามคิว อันไหนมาก่อนเปิดก่อน มาหลังเปิดหลัง
            File.WriteAllText("camera_show_list.txt", "");
        }
    }
    class Function_in_excel {
        public Function_in_excel(fMain f1, string strCMD) {
            fMain = f1;
            alice = fMain.excel.alice;
            eval(strCMD);
        }

        #region ===============================================Define====================================================
        private fMain fMain;
        private static PSU psu;
        private static Connect DcPSU;
        private SetFile setFile = new SetFile();

        static string[] alice = new string[5];
        private List<USBClassLibrary.USBClass.DeviceProperties> pidList;
        #endregion

        #region ===============================================Function Support====================================================
        private void define_class() {
            psu = new PSU();
            DcPSU = new Connect();
        }
        private void getNameDMM_visa() {
            Ivi.Visa.Interop.ResourceManager visa = new Ivi.Visa.Interop.ResourceManager();

            try
            {
                string[] visaList = visa.FindRsrc("?*");

                foreach (string list in visaList)
                {

                    if (list.Contains(fMain.configTester.nameDMM))
                    {
                        psu.nameDMM = list;
                    }
                }

            } catch
            {
                fMain.Log(fMain.LogMsgType.Incoming_Blue, psu.messageErr.fineNameDMM);
                return;
            }

            if (psu.nameDMM == "" || psu.nameDMM == null)
            {
                psu.nameDMM = string.Empty;
                fMain.Log(fMain.LogMsgType.Incoming_Blue, psu.messageErr.fineNameDMM);
            }
        }
        private void getNameOSC_visa() {
            Ivi.Visa.Interop.ResourceManager visa = new Ivi.Visa.Interop.ResourceManager();

            try
            {
                string[] visaList = visa.FindRsrc("?*");

                foreach (string list in visaList)
                {

                    if (list.Contains(psu.define.nameOSC))
                    {
                        psu.nameOSC = list;
                    }
                }

            } catch
            {
                fMain.Log(fMain.LogMsgType.Incoming_Blue, psu.messageErr.fineNameOSC);
                return;
            }

            if (psu.nameOSC == "" || psu.nameOSC == null)
            {
                psu.nameOSC = string.Empty;
                fMain.Log(fMain.LogMsgType.Incoming_Blue, psu.messageErr.fineNameOSC);
            }
        }
        private void checkDMMconnect_visa() {
            fMain.bt_reserve1.Text = psu.define.DMM;

            if (DcPSU.ConnectInstr(psu.nameDMM) == psu.define.connect)
            {
                fMain.bt_reserve1.BackColor = Color.LimeGreen;
                DcPSU.DisConnectInstr();

            }
            else
            {
                fMain.bt_reserve1.BackColor = Color.Red;
            }
        }
        private void checkOSCconnect_visa() {
            fMain.bt_reserve2.Text = psu.define.OSC;

            if (DcPSU.ConnectInstr(psu.nameOSC) == psu.define.connect)
            {
                fMain.bt_reserve2.BackColor = Color.LimeGreen;
                DcPSU.DisConnectInstr();

            }
            else
            {
                fMain.bt_reserve2.BackColor = Color.Red;
            }
        }
        private void checkOSCconnect_deviceComport() {
            fMain.bt_reserve2.Text = psu.define.OSC;
            fMain.bt_reserve2.BackColor = Color.Red;

            ManagementObjectSearcher getComport = new ManagementObjectSearcher(psu.define.deviceGet);
            ManagementObjectCollection getComportAll = getComport.Get();

            foreach (ManagementObject nameList in getComportAll)
            {

                if (!nameList[psu.define.deviceDescription].ToString().Contains(psu.define.TF960))
                    continue;

                string[] nameComport = nameList.GetPropertyValue(psu.define.deviceName).ToString().Split('(', ')');
                psu.nameOSC = nameComport[1];

                fMain.bt_reserve2.BackColor = Color.LimeGreen;
            }
        }
        private void checkOSCconnect_pid() {
            fMain.bt_reserve2.Text = psu.define.OSC;

            pidList = new List<USBClass.DeviceProperties>();

            if (USBClass.GetUSBDevice(uint.Parse(psu.define.vidOSC, System.Globalization.NumberStyles.AllowHexSpecifier),
                uint.Parse(psu.define.pidOSC, System.Globalization.NumberStyles.AllowHexSpecifier), ref pidList, true, null))
            {

                psu.nameOSC = pidList[0].COMPort;
                fMain.bt_reserve2.BackColor = Color.LimeGreen;

            }
            else
            {
                fMain.bt_reserve2.BackColor = Color.Red;
            }
        }

        public void PrismCheckProcess() {
            fMain.UpdateResultToDataGrid(alice[0], alice[2], fMain.define.pass);

            if (fMain.configPrism.mode != fMain.configPrism.Debug)
            {
                TextBox textBox = fMain.getTextBoxSN(fMain.select_test);

                string[] status = TeamPrecision.PRISM.cSNs.CheckStatusSNv2(textBox.Text, fMain.tb_wo.Text);

                //ตรวจสอบว่ามันตอบกลับด้วย "SUCCESS" ไหม
                if (status[0] == fMain.configPrism.success)
                {
                    if (status[1] == textBox.Text)
                    {
                        fMain.flag_sn_pass[fMain.select_test - 1] = true;
                        return;
                    }
                }

                //ถ้ามันตอบกลับว่า ยังไม่ผ่านการเทส process ก่อนหน้า โปรแกรมจะแสตมป์ fail และหยุดเทสทันที
                if (status[1].Contains(fMain.configPrism.processBeforeText))
                {
                    //แก้บัค กรณีเจอ Process มี จุด เช่น FCT2.1 สถานะ Fail
                    if (!status[1].Contains($"{fMain.configPrism.processBeforeText}."))
                    {
                        fMain.Log(fMain.LogMsgType.Error_Red, "\n" + status[1]);
                        fMain.UpdateResultToDataGrid(alice[0], status[1], fMain.define.fail);
                        return;
                    }
                }

                fMain.Log(fMain.LogMsgType.Incoming_Blue, "\n" + status[1]);

                //ถ้ามันตอบกลับว่า เทสไปแล้วแต่อยู่ใน สถานะ fail มันก็จะให้เทสได้
                if (status[1].Contains(fMain.prism_retest_text_fail.Text))
                {
                    fMain.flag_sn_pass[fMain.select_test - 1] = true;
                    return;
                }

                //ถ้ามันตอบกลับว่่า เทสผ่านไปแล้ว มันจะเช็คอีกว่า ยอมให้เทสซ้ำไหมตัวที่ผ่านแล้ว ถ้าได้ก็ให้เทส
                if (status[1].Contains(fMain.prism_retest_text_pass.Text))
                {
                    if (fMain.prism_retest.Checked)
                    {
                        fMain.flag_sn_pass[fMain.select_test - 1] = true;
                        return;

                    }
                    else
                    {
                        //ถ้าไม่อยากโชว์ popup ให้เอา if นี้ออก
                        if (CallFormReTest())
                        {
                            fMain.flag_sn_pass[fMain.select_test - 1] = true;
                            return;
                        }

                        fMain.flagNotReTest[fMain.select_test - 1] = true;
                        fMain.row_test[fMain.select_test - 1] += 10000;
                        return;
                    }
                }

                //ถ้าไม่เข้าเงื่อนไข อะไรเลย จะเพิ่มข้อความ "_SN" ต่อท้ายไปใน sn เดิม และแสตมป์ fail ด้วย
                fMain.UpdateResultToDataGrid(alice[0], status[1], fMain.define.fail);
                fMain.Log(fMain.LogMsgType.Error_Red, $"\n{status[1]} ไม่เข้าเงื่อนไข");
            }
        }
        public void PrismCheckProcessV2() {
            fMain.UpdateResultToDataGrid(alice[0], alice[2], fMain.define.pass);

            if (fMain.configPrism.mode != fMain.configPrism.Debug)
            {
                TextBox textBox = fMain.getTextBoxSN(fMain.select_test);

                string[] status = TeamPrecision.PRISM.cSNs.sn_check_valid(textBox.Text, fMain.tb_wo.Text, fMain.configPrism.processName, true);

                //ตรวจสอบว่ามันตอบกลับด้วย "PASS" ไหม
                if (status[0] == "PASS")
                {
                    fMain.flag_sn_pass[fMain.select_test - 1] = true;
                    return;
                }

                //ถ้ามันตอบกลับว่า ยังไม่ผ่านการเทส process ก่อนหน้า โปรแกรมจะแสตมป์ fail และหยุดเทสทันที
                if (status[0].Contains(fMain.configPrism.processBeforeText))
                {
                    //แก้บัค กรณีเจอ Process มี จุด เช่น FCT2.1 สถานะ Fail
                    if (!status[0].Contains($"{fMain.configPrism.processBeforeText}."))
                    {
                        fMain.Log(fMain.LogMsgType.Error_Red, "\n" + status[0]);
                        fMain.UpdateResultToDataGrid(alice[0], status[0], fMain.define.fail);
                        return;
                    }
                }

                fMain.Log(fMain.LogMsgType.Incoming_Blue, "\n" + status[0]);

                //ถ้ามันตอบกลับว่า เทสไปแล้วแต่อยู่ใน สถานะ fail มันก็จะให้เทสได้
                if (status[0].Contains(fMain.prism_retest_text_fail.Text))
                {
                    fMain.flag_sn_pass[fMain.select_test - 1] = true;
                    return;
                }

                //ถ้ามันตอบกลับว่่า เทสผ่านไปแล้ว มันจะเช็คอีกว่า ยอมให้เทสซ้ำไหมตัวที่ผ่านแล้ว ถ้าได้ก็ให้เทส
                if (status[0].Contains(fMain.prism_retest_text_pass.Text))
                {
                    if (fMain.prism_retest.Checked)
                    {
                        fMain.flag_sn_pass[fMain.select_test - 1] = true;
                        return;

                    }
                    else
                    {
                        //ถ้าไม่อยากโชว์ popup ให้เอา if นี้ออก
                        if (CallFormReTest())
                        {
                            fMain.flag_sn_pass[fMain.select_test - 1] = true;
                            return;
                        }

                        fMain.flagNotReTest[fMain.select_test - 1] = true;
                        fMain.row_test[fMain.select_test - 1] += 10000;
                        return;
                    }
                }


                //ถ้าไม่เข้าเงื่อนไข อะไรเลย จะเพิ่มข้อความ "_SN" ต่อท้ายไปใน sn เดิม และแสตมป์ fail ด้วย
                fMain.UpdateResultToDataGrid(alice[0], status[0], fMain.define.fail);
            }
        }

        private void GenFunctionToCSV() {
            List<string> function = new List<string>();

            foreach (MemberInfo memberInfo in this.GetType().GetMembers())
            {

                if (memberInfo.Name == ".ctor" || memberInfo.Name == "Equals" || memberInfo.Name == "GetHashCode" ||
                    memberInfo.Name == "GetType" || memberInfo.Name == "ToString" || memberInfo.Name == "LoadTestSpec" ||
                    memberInfo.Name == "OnAllFunction" || memberInfo.Name == "intro_test" || memberInfo.Name == "after_test" ||
                    memberInfo.Name == "OffAllFunction")
                {
                    continue;
                }

                string nameFunction = memberInfo.Name;
                string parameter = "";

                foreach (ParameterInfo parameterInfo in ((MethodInfo)memberInfo).GetParameters())
                {
                    parameter += "," + parameterInfo.Name;
                }

                function.Add(nameFunction + parameter);
            }

            File.WriteAllLines("AllFunctionInClass.csv", function);
        }

        public void camera_set_step(string cmd = "read2d") {
            File.WriteAllText("../../config/test_head_" + fMain.select_test + "_steptest.txt", cmd);
            File.Delete("test_head_" + fMain.select_test + "_result.txt");
        }
        public void set_timeout(string cmd = "1000") {
            File.WriteAllText("../../config/test_head_" + fMain.select_test + "_timeout.txt", cmd);
        }
        public void camera_set_list() {
            while (true)
            {
                try
                {
                    File.AppendAllText("camera_show_list.txt", fMain.select_test.ToString());
                    break;
                } catch { }
                fMain.DelaymS(250);
            }
        }

        private void SetPortCameraCsv(string fileTarget, string head, string port) {
            string[] dataConfig = File.ReadAllLines(fileTarget);

            for (int loop = 0; loop < dataConfig.Length; loop++)
            {
                if (dataConfig[loop].StartsWith($"Head {head} Port"))
                {
                    string[] parts = dataConfig[loop].Split(',');
                    if (parts.Length == 2 && int.TryParse(parts[1], out int value))
                    {
                        dataConfig[loop] = $"Head {head} Port,{port}";
                        // หยุดลูปหลังจากพบค่าและทำการเปลี่ยนแล้ว
                        break;
                    }
                }
            }

            File.WriteAllLines(fileTarget, dataConfig);
        }
        private void ShowMessage(Form formScan, string message) {
            formScan.FormBorderStyle = FormBorderStyle.FixedSingle;
            formScan.MaximizeBox = false;
            formScan.Text = "Put Unit Error";
            formScan.StartPosition = FormStartPosition.CenterScreen;
            formScan.Size = new Size(1000, 400);
            formScan.BackColor = Color.Red;
            Label label = new Label();
            label.Text = $"{message}";
            label.ForeColor = Color.White;
            label.Size = new Size(900, 350);
            label.Location = new Point(50, 70);
            FontFamily fontFamily = new FontFamily("Yu Gothic UI");
            label.Font = new Font(fontFamily, 130, FontStyle.Bold, GraphicsUnit.Pixel);

            formScan.Controls.Add(label);
            formScan.Show();

            fMain.DelaymS(150);
        }


        // Run Script
        private void RunScript(string nameFile) {
            string username = Environment.UserName; // Retrieve the current username
            string pythonPath = $@"C:\Users\{username}\AppData\Local\Programs\Python\{fMain.configScript.pyVersion}\{fMain.configScript.pyFolder}\python.exe";
            string scriptPath = $"../../Script/{nameFile}.py";

            // Build the command to be executed
            string arguments = $"\"{scriptPath}\"";

            // Create a new process to execute the Python script
            Process process = new Process();
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.FileName = pythonPath;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = false;

            try
            {
                process.Start();

                //string output = process.StandardOutput.ReadToEnd();
                //string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                //string lastLine = lines[lines.Length - 1];

                //process.WaitForExit();

                //return lastLine;
            } catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                //return "Error";
            }
        }
        private string RunScriptWait(string nameFile, string folder) {
            string username = Environment.UserName; // Retrieve the current username
            string pythonPath = $@"C:\Users\{username}\AppData\Local\Programs\Python\{fMain.configScript.pyVersion}\{fMain.configScript.pyFolder}\python.exe";
            string scriptPath = $"../../Script/{folder}/{nameFile}.py";

            // Build the command to be executed
            string arguments = $"\"{scriptPath}\"";

            // Create a new process to execute the Python script
            Process process = new Process();
            process.StartInfo.CreateNoWindow = !fMain.ctms_showCmd.Checked;
            process.StartInfo.FileName = pythonPath;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;

            try
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string lastLine = lines[lines.Length - 1];

                process.WaitForExit();

                return lastLine;
            } catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return "Error";
            }
        }
        private string RunScriptWait2(string nameFile, string folder) {
            string username = Environment.UserName; // Retrieve the current username
            string pythonPath = $@"C:\Users\{username}\AppData\Local\Programs\Python\{fMain.configScript.pyVersion}\{fMain.configScript.pyFolder}\python.exe";
            string scriptPath = $"../../Script/{folder}/{nameFile}.py";

            // Build the command to be executed
            string arguments = $"\"{scriptPath}\"";

            // Create a new process to execute the Python script
            Process process = new Process();
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = pythonPath;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;

            try
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string lastLine = lines[lines.Length - 1];

                process.WaitForExit();

                return lastLine;
            } catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return "Error";
            }
        }
        private string RunScriptWait(string nameFile, string folder, string argument1) {
            string username = Environment.UserName; // Retrieve the current username
            string pythonPath = $@"C:\Users\{username}\AppData\Local\Programs\Python\{fMain.configScript.pyVersion}\{fMain.configScript.pyFolder}\python.exe";
            string scriptPath = $"../../Script/{folder}/{nameFile}.py";

            // Build the command to be executed
            string arguments = $"\"{scriptPath}\" \"{argument1}\"";

            // Create a new process to execute the Python script
            Process process = new Process();
            process.StartInfo.CreateNoWindow = !fMain.ctms_showCmd.Checked;
            process.StartInfo.FileName = pythonPath;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;

            try
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string lastLine = lines[lines.Length - 1];

                process.WaitForExit();

                return lastLine;
            } catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return "Error";
            }
        }
        private string RunScriptWait2(string nameFile, string folder, string argument1) {
            string username = Environment.UserName; // Retrieve the current username
            string pythonPath = $@"C:\Users\{username}\AppData\Local\Programs\Python\{fMain.configScript.pyVersion}\{fMain.configScript.pyFolder}\python.exe";
            string scriptPath = $"../../Script/{folder}/{nameFile}.py";

            // Build the command to be executed
            string arguments = $"\"{scriptPath}\" \"{argument1}\"";

            // Create a new process to execute the Python script
            Process process = new Process();
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = pythonPath;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;

            try
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string lastLine = lines[lines.Length - 1];

                process.WaitForExit();

                return lastLine;
            } catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return "Error";
            }
        }
        private string RunScriptWait(string nameFile, string folder, string argument1, string argument2) {
            string username = Environment.UserName; // Retrieve the current username
            string pythonPath = $@"C:\Users\{username}\AppData\Local\Programs\Python\{fMain.configScript.pyVersion}\{fMain.configScript.pyFolder}\python.exe";
            string scriptPath = $"../../Script/{folder}/{nameFile}.py";

            // Build the command to be executed
            string arguments = $"\"{scriptPath}\" \"{argument1}\" \"{argument2}\"";

            // Create a new process to execute the Python script
            Process process = new Process();
            process.StartInfo.CreateNoWindow = !fMain.ctms_showCmd.Checked;
            process.StartInfo.FileName = pythonPath;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;

            try
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string lastLine = lines[lines.Length - 1];

                process.WaitForExit();

                return lastLine;
            } catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return "Error";
            }
        }
        private string RunScriptWait2(string nameFile, string folder, string argument1, string argument2) {
            string username = Environment.UserName; // Retrieve the current username
            string pythonPath = $@"C:\Users\{username}\AppData\Local\Programs\Python\{fMain.configScript.pyVersion}\{fMain.configScript.pyFolder}\python.exe";
            string scriptPath = $"../../Script/{folder}/{nameFile}.py";

            // Build the command to be executed
            string arguments = $"\"{scriptPath}\" \"{argument1}\" \"{argument2}\"";

            // Create a new process to execute the Python script
            Process process = new Process();
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = pythonPath;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;

            try
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string lastLine = lines[lines.Length - 1];

                process.WaitForExit();

                return lastLine;
            } catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return "Error";
            }
        }
        private string RunScriptWait(string nameFile, string folder, string argument1, string argument2, string argument3) {
            string username = Environment.UserName; // Retrieve the current username
            string pythonPath = $@"C:\Users\{username}\AppData\Local\Programs\Python\{fMain.configScript.pyVersion}\{fMain.configScript.pyFolder}\python.exe";
            string scriptPath = $"../../Script/{folder}/{nameFile}.py";

            // Build the command to be executed
            string arguments = $"\"{scriptPath}\" \"{argument1}\" \"{argument2}\" \"{argument3}\"";

            // Create a new process to execute the Python script
            Process process = new Process();
            process.StartInfo.CreateNoWindow = !fMain.ctms_showCmd.Checked;
            process.StartInfo.FileName = pythonPath;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;

            try
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string lastLine = lines[lines.Length - 1];

                process.WaitForExit();

                return lastLine;
            } catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return "Error";
            }
        }


        //Find Port
        private static string PortDmm;
        private void FindPortDmm() {
            string result = RunScriptWait("FindPort", "U3606");
            fMain.bt_reserve1.Text = "DMM";

            if (result.Contains("0x0957::0x4D18"))
            {
                PortDmm = result;
                fMain.bt_reserve1.BackColor = Color.LimeGreen;
            }
            else
            {
                fMain.bt_reserve1.BackColor = Color.Red;
            }
        }


        // Measure
        public void MeasureResistor(string rang = "5000", string timeout = "1000") {
            double min_ = Convert.ToDouble(alice[2]);
            double max_ = Convert.ToDouble(alice[3]);
            double value = 0;
            double timeout_ = Convert.ToDouble(timeout);
            bool passed = false; // Flag to indicate if the measurement passed

            Stopwatch time = Stopwatch.StartNew(); // Start the Stopwatch directly.

            while (time.ElapsedMilliseconds < timeout_)
            {
                string valueString = RunScriptWait("MeasureResistance", "U3606", PortDmm, rang);

                if (valueString.Contains("VISA error"))
                {
                    string dataSup = RunScriptWait("ClearError", "U3606", PortDmm);
                    continue;
                }

                value = Convert.ToDouble(valueString);
                if (value > max_ || value < min_)
                {
                    fMain.UpdateResultToDataGrid(alice[0], value.ToString(), "FAIL");
                    fMain.DelaymS(50);
                    continue;
                }

                fMain.UpdateResultToDataGrid(alice[0], value.ToString(), "PASS");
                passed = true; // Measurement passed
                break;
            }

            if (!passed)
            {
                fMain.UpdateResultToDataGrid(alice[0], value.ToString(), "FAIL"); // Update with the last measured value if it failed
            }

        }


        // Skip
        public void SkipPass()
        {
            fMain.UpdateResultToDataGrid(alice[0], alice[2], "PASS");
        }
        #endregion

        #region ===============================================Function Main====================================================
        public void delay_ms(string time) {
            fMain.DelaymS(Convert.ToInt32(time));
        }

        public void test_pass() {
            fMain.UpdateResultToDataGrid(alice[0], "PASS", "PASS");
        }
        public void test_fail() {
            fMain.UpdateResultToDataGrid(alice[0], "FAIL", "FAIL");
        }
        public void test_write(string data) {
            fMain.UpdateResultToDataGrid(alice[0], data, "PASS");
        }

        private TextBox getTextBox() {
            TextBox textBox = fMain.getTextBoxSN(fMain.select_test);

            return textBox;
        }
        private DataGridView getDataGridView() {
            DataGridView dataGridView = fMain.getDataGridView(fMain.select_test);

            return dataGridView;
        }
        public void Camera_set_sn_to_textbox() {
            fMain.flag_sn_pass[fMain.select_test - 1] = false;

            TextBox t = fMain.getTextBoxSN(fMain.select_test);
            t.Text = serialTeamSup[fMain.select_test - 1];


            //DataGridView d = fMain.getDataGridView(fMain.select_test);

            //for (int i = 0; i < d.Rows.Count; i++)
            //{

            //    if (d.Rows[i].Cells[0].Value.ToString() != alice[0])
            //        continue;

            //    t.Text = d.Rows[i].Cells[3].Value.ToString();

            //    //CheckSN_DLL(t);

            //    break;
            //}
        }
        public void Camera_clear_sn_in_textbox() {
            TextBox t = new TextBox();
            switch (fMain.select_test)
            {
                case 1:
                    t = fMain.txtSNBoard_1;
                    break;
                case 2:
                    t = fMain.txtSNBoard_2;
                    break;
                case 3:
                    t = fMain.txtSNBoard_3;
                    break;
                case 4:
                    t = fMain.txtSNBoard_4;
                    break;
                case 5:
                    t = fMain.txtSNBoard_5;
                    break;
                case 6:
                    t = fMain.txtSNBoard_6;
                    break;
                case 7:
                    t = fMain.txtSNBoard_7;
                    break;
                case 8:
                    t = fMain.txtSNBoard_8;
                    break;
                case 9:
                    t = fMain.txtSNBoard_9;
                    break;
                case 10:
                    t = fMain.txtSNBoard_10;
                    break;
                case 11:
                    t = fMain.txtSNBoard_11;
                    break;
                case 12:
                    t = fMain.txtSNBoard_12;
                    break;
                case 13:
                    t = fMain.txtSNBoard_13;
                    break;
                case 14:
                    t = fMain.txtSNBoard_14;
                    break;
                case 15:
                    t = fMain.txtSNBoard_15;
                    break;
                case 16:
                    t = fMain.txtSNBoard_16;
                    break;
                case 17:
                    t = fMain.txtSNBoard_17;
                    break;
                case 18:
                    t = fMain.txtSNBoard_18;
                    break;
                case 19:
                    t = fMain.txtSNBoard_19;
                    break;
                case 20:
                    t = fMain.txtSNBoard_20;
                    break;
                case 21:
                    t = fMain.txtSNBoard_21;
                    break;
                case 22:
                    t = fMain.txtSNBoard_22;
                    break;
                case 23:
                    t = fMain.txtSNBoard_23;
                    break;
                case 24:
                    t = fMain.txtSNBoard_24;
                    break;
                case 25:
                    t = fMain.txtSNBoard_25;
                    break;
                case 26:
                    t = fMain.txtSNBoard_26;
                    break;
                case 27:
                    t = fMain.txtSNBoard_27;
                    break;
                case 28:
                    t = fMain.txtSNBoard_28;
                    break;
                case 29:
                    t = fMain.txtSNBoard_29;
                    break;
                case 30:
                    t = fMain.txtSNBoard_30;
                    break;
                case 31:
                    t = fMain.txtSNBoard_31;
                    break;
                case 32:
                    t = fMain.txtSNBoard_32;
                    break;
                case 33:
                    t = fMain.txtSNBoard_33;
                    break;
                case 34:
                    t = fMain.txtSNBoard_34;
                    break;
                case 35:
                    t = fMain.txtSNBoard_35;
                    break;
                case 36:
                    t = fMain.txtSNBoard_36;
                    break;
            }
            t.Text = "";
        }
        public void ScannerGetFile() {
            string sn = string.Empty;
            bool flagReadSN = false;
            try
            {
                sn = File.ReadAllText("dryice_scan2d_sn_header_" + fMain.select_test + ".txt");
                flagReadSN = true;
            } catch { }
            if (flagReadSN)
            {
                fMain.UpdateResultToDataGrid(alice[0], sn, "PASS");
            }
            else
            {
                fMain.UpdateResultToDataGrid(alice[0], "Not Found SN", "FAIL");
                ScanSnPutUnitError();
            }
            File.Delete("dryice_scan2d_sn_header_" + fMain.select_test + ".txt");
        }
        public void lightOnRead2D() {
            fMain.write_23017($"rgbset,{fMain.select_test},0,100,100,100\n");
        }
        public void lightOffRead2D() {
            fMain.write_23017($"rgbset,{fMain.select_test},0,0,0,0\n");
        }
        private static bool[] waitTextBarCode = new bool[36];
        private static string[] serialTeamSup = new string[36];
        public void Read2DBarCode() {
            if (!waitTextBarCode[fMain.select_test - 1])
            {
                Camera_clear_sn_in_textbox();
            }

            if (fMain.configUI.readBarcode == ConfigUI.ReadBarcode.Camera)
            {
                string pathResult = $"test_head_{fMain.select_test}_result.txt";

                if (waitTextBarCode[fMain.select_test - 1])
                {
                    try
                    {
                        if (File.Exists(pathResult))
                        {
                            string[] dataCamera = File.ReadAllLines(pathResult);
                            File.Delete(pathResult);
                            fMain.UpdateResultToDataGrid(alice[0], dataCamera[0], dataCamera[1]);
                            serialTeamSup[fMain.select_test - 1] = dataCamera[0];
                            lightOffRead2D();
                            ProgressBar progressBar = fMain.getProgressBar(fMain.select_test);
                            progressBar.Value--;
                            return;
                        }
                    } catch { }

                    ProgressBar progressBar_ = fMain.getProgressBar(fMain.select_test);
                    progressBar_.Value--;
                    fMain.row_test[fMain.select_test - 1]--;
                    return;
                }

                File.WriteAllText("../../config/head.txt", fMain.select_test.ToString());
                camera_set_step("read2d");
                camera_set_list();
                set_timeout("5000");
                lightOnRead2D();

                Process process = new Process();
                process.StartInfo.CreateNoWindow = false;
                process.StartInfo.FileName = $"../../mini_projeck/camera_show.exe";
                process.StartInfo.UseShellExecute = false;

                File.Delete(pathResult);
                File.Delete("call_exe_tric.txt");

                try
                {
                    process.Start();
                    waitTextBarCode[fMain.select_test - 1] = true;
                } catch (Exception ex)
                {
                    // MessageBox.Show("Error: " + ex.Message);
                }

                while (true)
                {
                    try
                    { 
                        File.ReadAllText("call_exe_tric.txt"); 
                        break; 
                    } catch { }
                    fMain.DelaymS(50);
                }

                fMain.row_test[fMain.select_test - 1]--;
            }

            if (fMain.configUI.readBarcode == ConfigUI.ReadBarcode.Scanner)
            {
                ScannerGetFile();
            }
        }


        /// <summary>
        /// Put unit error to show popup
        /// </summary>
        private void ScanSnPutUnitError() {
            Form formScan = new Form();
            formScan.FormBorderStyle = FormBorderStyle.FixedSingle;
            formScan.MaximizeBox = false;
            formScan.Text = "Put Unit Error";
            formScan.StartPosition = FormStartPosition.CenterScreen;
            formScan.Size = new Size(1000, 400);
            formScan.BackColor = Color.Red;
            Label label = new Label();
            label.Text = "ใส่งานผิดหัว";
            label.ForeColor = Color.White;
            label.Size = new Size(900, 350);
            label.Location = new Point(50, 70);
            FontFamily fontFamily = new FontFamily("Yu Gothic UI");
            label.Font = new Font(fontFamily, 150, FontStyle.Bold, GraphicsUnit.Pixel);

            formScan.Controls.Add(label);
            formScan.ShowDialog();
        }

        private bool CallFormReTest_() {
            DialogResult dialogResult = MessageBox.Show("Want to repeat the test?", "ReTest", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                return true;
            }

            return false;
        }
        private Form formTest = new Form();
        private bool flagReTestPopUp;
        private bool CallFormReTest() {
            flagReTestPopUp = false;
            formTest.Size = new Size(710, 300);
            formTest.FormBorderStyle = FormBorderStyle.FixedSingle;
            formTest.StartPosition = FormStartPosition.CenterScreen;
            formTest.MaximizeBox = false;
            formTest.Text = fMain.select_test + ".ReTest";
            Label lb_head = new Label();
            lb_head.Text = "Head";
            lb_head.ForeColor = Color.Brown;
            lb_head.Location = new Point(0, 0);
            FontFamily fontFamily = new FontFamily("Arial");
            lb_head.Font = new Font(fontFamily, 70, FontStyle.Bold, GraphicsUnit.Pixel);
            lb_head.AutoSize = true;
            Label lb_number = new Label();
            lb_number.Text = fMain.select_test.ToString();
            lb_number.ForeColor = Color.Blue;
            lb_number.Location = new Point(10, 60);
            lb_number.Font = new Font(fontFamily, 200, FontStyle.Bold, GraphicsUnit.Pixel);
            lb_number.AutoSize = true;
            Label lb_detail = new Label();
            lb_detail.Text = "Want to repeat the test?";
            lb_detail.Font = new Font(fontFamily, 20, FontStyle.Bold, GraphicsUnit.Pixel);
            lb_detail.AutoSize = true;
            lb_detail.Location = new Point(350, 50);
            Button bt_reTest = new Button();
            bt_reTest.Click += ButtonReTestClick;
            bt_reTest.Text = "ReTest";
            bt_reTest.Font = new Font(fontFamily, 30, FontStyle.Bold, GraphicsUnit.Pixel);
            bt_reTest.Location = new Point(250, 150);
            bt_reTest.Size = new Size(200, 60);
            Button bt_cancel = new Button();
            bt_cancel.Click += ButtonCancelClick;
            bt_cancel.Text = "Cancel";
            bt_cancel.Font = new Font(fontFamily, 30, FontStyle.Bold, GraphicsUnit.Pixel);
            bt_cancel.Location = new Point(470, 150);
            bt_cancel.Size = new Size(200, 60);

            formTest.Controls.Add(lb_head);
            formTest.Controls.Add(lb_number);
            formTest.Controls.Add(lb_detail);
            formTest.Controls.Add(bt_reTest);
            formTest.Controls.Add(bt_cancel);
            formTest.ShowDialog();

            if (flagReTestPopUp)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void ButtonReTestClick(object sender, EventArgs e) {
            flagReTestPopUp = true;
            formTest.Close();
        }
        private void ButtonCancelClick(object sender, EventArgs e) {
            formTest.Close();
        }
        #endregion

        #region ================================================EXCEL========================================================
        private void eval(string string_of_function) {
            string name_function = "";
            List<string> parameter = new List<string>();
            string support_parameter = "";
            int num_parameter = 0;
            try
            {
                for (int i = 0; i < string_of_function.Length; i++)
                {
                    if (string_of_function.Substring(i, 1) == "(")
                    {
                        break;
                    }
                    name_function += string_of_function.Substring(i, 1);
                }
                for (int i = name_function.Length + 1; i < string_of_function.Length; i++)
                {
                    if (string_of_function.Substring(i, 1) != "=")
                    {
                        continue;
                    }
                    for (int j = i + 1; j < string_of_function.Length; j++)
                    {
                        if (string_of_function.Substring(j, 1) == "," || string_of_function.Substring(j, 1) == ")")
                        {
                            num_parameter++;
                            i = j;
                            parameter.Add(support_parameter);
                            support_parameter = "";
                            goto label_num_parameter;
                        }
                        support_parameter += string_of_function.Substring(j, 1);
                    }
                label_num_parameter:
                    ;
                }
            } catch (Exception)
            {
                MessageBox.Show("header " + fMain.select_test + " function error " + string_of_function);
                fMain.UpdateResultToDataGrid(alice[0], "", "FAIL");
                return;
            }
            for (int reHead = 0; reHead <= 1; reHead++)
            {
                object[] objects = parameter.ConvertAll<object>(item => (object)item).ToArray();
                MethodInfo mi = this.GetType().GetMethod(name_function);
                try
                {
                    mi.Invoke(this, objects);
                    break;
                } catch
                {
                    if (reHead == 0)
                    {
                        string headSup = name_function.Substring(0, 1);
                        string tailSup = name_function.Substring(1, name_function.Length - 1);
                        if (headSup.Any(char.IsUpper))
                        {
                            headSup = headSup.ToLower();
                        }
                        else
                        {
                            headSup = headSup.ToUpper();
                        }
                        name_function = headSup + tailSup;
                        continue;
                    }
                    MessageBox.Show("header " + fMain.select_test + " function error " + string_of_function);
                    fMain.UpdateResultToDataGrid(alice[0], "", "FAIL");
                    return;
                }
            }
        }
        #endregion

        public void LoadTestSpec() {
            //Create Form Show Message
            Form form = new Form();
            ShowMessage(form, "โปรดรอสักครู่");


            //getNameDMM_visa();
            //psu.define.nameOSC = fMain.setupPay.read_text("Name OSC", "tester_config");
            //getNameOSC_visa();

            //checkDMMconnect_visa();
            //checkOSCconnect_visa();
            //checkOSCconnect_deviceComport();
            //checkOSCconnect_pid();



            form.Close();
        }//ชังก์ชั่นนี้จะทำงานตอน เลือก FG
        public void setup() {
            define_class();
            setFile.cameraList();




        } //ฟังก์ชั่นนี้จะทำงานครั้งเดียวหลังจากเปิดโปรแกรมเทส
        public void OnAllFunction() {

        } //ฟังก์ชั่นนี้จะทำงาน ก่อน เริ่มเทสทั้งพาแนล *แต่ห้ามควบคุม relay card ในนี้
        public void intro_test() {
            waitTextBarCode[fMain.select_test - 1] = false;
        } //ฟังก์ชั่นนี้จะทำงานก่อนเริ่มเทสหัวใดหัวหนึ่ง
        public void after_test() {
            //fMain.write_23017("rgbset,0,0,0,0,0\n");

            //fMain.OffAllCard(fMain.select_test);
            //fMain.Relay_Off(fMain.select_test, fMain.bit1);
            //fMain.Relay_Off(fMain.select_test, fMain.bit2);
            //fMain.Relay_Off(fMain.select_test, fMain.bit3);
            //fMain.Relay_Off(fMain.select_test, fMain.bit4);
            //fMain.Relay_Off(fMain.select_test, fMain.bit5);
            //fMain.Relay_Off(fMain.select_test, fMain.bit6);
            //fMain.Relay_Off(fMain.select_test, fMain.bit7);
            //fMain.Relay_Off(fMain.select_test, fMain.bit8);
            //fMain.Relay_Off(fMain.select_test, fMain.bit9);
            //fMain.Relay_Off(fMain.select_test, fMain.bit10);
            //fMain.Relay_Off(fMain.select_test, fMain.bit11);
            //fMain.Relay_Off(fMain.select_test, fMain.bit12);
            //fMain.Relay_Off(fMain.select_test, fMain.bit13);
            //fMain.Relay_Off(fMain.select_test, fMain.bit14);
            //fMain.Relay_Off(fMain.select_test, fMain.bit15);
            //fMain.Relay_Off(fMain.select_test, fMain.bit16);

            GetStatusResult();
        }  //ฟังก์ชั่นนี้จะทำงานหลังจากเทสหัวใดหัวหนึ่งเสร็จ
        public void OffAllFunction() {
            if (fMain.configTester.useRelayCard)
            {

            }


        }   //ฟังก์ชั่นนี้จะทำงาน หลัง เทสทั้งพาแนลเสร็จ *แต่ห้ามควบคุม relay card ในนี้
        public void SetPortCameraMain() {
            // Set Bottom
            string nameSetPort = "read2d_SetPort";  // ชื่อไฟล์ที่ใช้เก็บ config ของ SetPort Camera
            int numberHead = 4;  // จำนวนหัวของ Fixture
            int numberCamera = 4;   // จำนวนกล้อง
            string[] nameFileTarget = { "cameraStep_read2d" };   // ชื่อไฟล์ของเป้าหมายที่เราจะ Set Port ให้ถูกต้อง

            fMain.write_23017("rgbset,0,0,0,0,0\n");
            fMain.write_23017("rgbset,0,0,100,100,100\n");

            for (int loop = 1; loop <= numberCamera; loop++)
            {
                int port = loop - 1;
                File.WriteAllText($"../../config/test_head_{loop}_steptest.txt", nameSetPort);
                File.AppendAllText("camera_show_list.txt", $"{loop}");
                set_timeout("5000");
                SetPortCameraCsv($"../../config/cameraStep_{nameSetPort}.csv", $"{loop}", $"{port}");
                File.WriteAllText("../../config/head.txt", $"{loop}");

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = false;
                startInfo.UseShellExecute = false;
                startInfo.FileName = "../../mini_projeck/camera_show.exe";
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                File.Delete("call_exe_tric.txt");
                string path = $"test_head_{loop}_result.txt";
                File.Delete(path);
                try
                {
                    Process.Start(startInfo);
                } catch
                {
                    return;
                }
                string data2D;
                while (true)
                {
                    try
                    {
                        data2D = File.ReadAllText(path).Split('\n')[0];
                        break;
                    } catch { }
                    fMain.DelaymS(50);
                }
                // MessageBox.Show(data2D);
                if (!data2D.Contains("Bottom"))
                {
                    continue;
                }
                string resultPort = new string(data2D.Where(char.IsDigit).ToArray());
                foreach (string nameFile in nameFileTarget)
                {
                    // MessageBox.Show($"{nameFile}  -- {resultPort}  -- {port}");
                    SetPortCameraCsv($"../../config/{nameFile}.csv", resultPort, $"{port}");
                }
            }

            fMain.write_23017("rgbset,0,0,0,0,0\n");
            MessageBox.Show("Complete");
        }   // สำหรับ Auto หา Port Camera


        #region ============================================== Function User =====================================================

        public void TestTest() {
            fMain.MatrixOn(fMain.Matrix.Row1.Columns1);
        }

        public void GetStatusResult() {
            DataGridView dataGrid = GetDtaGridView();
            if (dataGrid.Rows.Count == 0)
            {
                MessageBox.Show("Get DataGridView Error");
            }
            bool results = true;
            for (int loop = 0; loop < dataGrid.Rows.Count - 1; loop++)
            {
                try
                {
                    if (dataGrid.Rows[loop].Cells[4].Value.ToString() != "PASS" && dataGrid.Rows[loop].Cells[4].Value.ToString() != "")
                    {
                        results = false;
                        break;
                    }
                } catch { results = false; break; }
            }
            if (results)
                File.WriteAllText("dryice_scan2d_tested_" + fMain.select_test + ".txt", "PASS");
            else
                File.WriteAllText("dryice_scan2d_tested_" + fMain.select_test + ".txt", "FAIL");
        }
        public DataGridView GetDtaGridView() {
            DataGridView dataGrid = new DataGridView();
            switch (fMain.select_test)
            {
                case 1:
                    dataGrid = fMain.dataGridView_1;
                    break;
                case 2:
                    dataGrid = fMain.dataGridView_2;
                    break;
                case 3:
                    dataGrid = fMain.dataGridView_3;
                    break;
                case 4:
                    dataGrid = fMain.dataGridView_4;
                    break;
                case 5:
                    dataGrid = fMain.dataGridView_5;
                    break;
                case 6:
                    dataGrid = fMain.dataGridView_6;
                    break;
                case 7:
                    dataGrid = fMain.dataGridView_7;
                    break;
                case 8:
                    dataGrid = fMain.dataGridView_8;
                    break;
                case 9:
                    dataGrid = fMain.dataGridView_9;
                    break;
                case 10:
                    dataGrid = fMain.dataGridView_10;
                    break;
                case 11:
                    dataGrid = fMain.dataGridView_11;
                    break;
                case 12:
                    dataGrid = fMain.dataGridView_12;
                    break;
                case 13:
                    dataGrid = fMain.dataGridView_13;
                    break;
                case 14:
                    dataGrid = fMain.dataGridView_14;
                    break;
                case 15:
                    dataGrid = fMain.dataGridView_15;
                    break;
                case 16:
                    dataGrid = fMain.dataGridView_16;
                    break;
                case 17:
                    dataGrid = fMain.dataGridView_17;
                    break;
                case 18:
                    dataGrid = fMain.dataGridView_18;
                    break;
                case 19:
                    dataGrid = fMain.dataGridView_19;
                    break;
                case 20:
                    dataGrid = fMain.dataGridView_20;
                    break;
                case 21:
                    dataGrid = fMain.dataGridView_21;
                    break;
                case 22:
                    dataGrid = fMain.dataGridView_22;
                    break;
                case 23:
                    dataGrid = fMain.dataGridView_23;
                    break;
                case 24:
                    dataGrid = fMain.dataGridView_24;
                    break;
                case 25:
                    dataGrid = fMain.dataGridView_25;
                    break;
                case 26:
                    dataGrid = fMain.dataGridView_26;
                    break;
                case 27:
                    dataGrid = fMain.dataGridView_27;
                    break;
                case 28:
                    dataGrid = fMain.dataGridView_28;
                    break;
                case 29:
                    dataGrid = fMain.dataGridView_29;
                    break;
                case 30:
                    dataGrid = fMain.dataGridView_30;
                    break;
                case 31:
                    dataGrid = fMain.dataGridView_31;
                    break;
                case 32:
                    dataGrid = fMain.dataGridView_32;
                    break;
                case 33:
                    dataGrid = fMain.dataGridView_33;
                    break;
                case 34:
                    dataGrid = fMain.dataGridView_34;
                    break;
                case 35:
                    dataGrid = fMain.dataGridView_35;
                    break;
                case 36:
                    dataGrid = fMain.dataGridView_36;
                    break;
            }
            return dataGrid;
        }


        public void delay_set_time_time(string cmd = "") {
            File.WriteAllText("../../config/delay_" + fMain.select_test + "_time.txt", cmd);
        }
        public void delay_display(string cmd = "") {
            File.WriteAllText("../../config/delay_" + fMain.select_test + "_display.txt", cmd);
        }
        public void GenCommandUpFw() {
            string nameProgramPath = "DryiceProgram_";
            string pathCommand = Path.Combine("D:\\PathZero\\Command", nameProgramPath);
            string pathResult = Path.Combine("D:\\PathZero\\Result", nameProgramPath);

            try
            {
                //Directory.CreateDirectory(pathCommand);
                File.Delete(pathResult + $"Result{fMain.select_test}.txt");

                File.WriteAllText(pathCommand + "Head.txt", fMain.select_test.ToString());
                File.WriteAllText(pathCommand + "CheckSum.txt", "0x0168EC74");
                File.WriteAllText(pathCommand + "Debug.txt", "False");
                File.WriteAllText(pathCommand + "Hex.txt", "TTULTRA_MU_16K_64K_v716.hex");
                if (fMain.select_test == 1)
                {
                    File.WriteAllText(pathCommand + "StLink.txt", "35001800120000334351534E");
                }
                else
                {
                    File.WriteAllText(pathCommand + "StLink.txt", "280019000B0000334351534E");
                }
                //File.WriteAllText(pathCommand + "StLink.txt", "35001800120000334351534E");
                File.WriteAllText(pathCommand + "TimeOut.txt", "30000");
                File.WriteAllText(pathCommand + "Start.txt", string.Empty);
            } catch (Exception ex)
            {
                // Handle any exception that may occur
                // Log or display an appropriate error message
            }

            string pathAck = pathResult + "Ack.txt";
            int timeout = 30000; // Timeout in milliseconds (30 seconds)
            DateTime startTime = DateTime.Now;

            while (true)
            {
                fMain.DelaymS(50);
                if (File.Exists(pathAck))
                {
                    try
                    {
                        string text = File.ReadAllText(pathAck);
                        File.Delete(pathAck);
                        break;
                    }catch (Exception ex)
                    {
                        continue;
                    }
                }

                TimeSpan elapsedTime = DateTime.Now - startTime;
                if (elapsedTime.TotalMilliseconds >= timeout)
                {
                    // Timeout reached, break the loop
                    //Console.WriteLine("Timeout occurred.");
                    fMain.UpdateResultToDataGrid(alice[0], "Ack file not found", "FAIL");
                    break;
                }
            }
        }



        public void AAA(string fff)
        {
            MessageBox.Show(fff);
        }
        public void BBB(string fff)
        {
            MessageBox.Show(fff);
        }

        #endregion

    }
}
