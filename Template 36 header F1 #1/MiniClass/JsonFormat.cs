﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class JsonFormat
{
    public string Header = "GeoBlue";
    public string Date = string.Empty;
    public string Time = string.Empty;
    public string Tester = string.Empty;
    public string Command = string.Empty;
    public Payload_ Payload = new Payload_();
    public string CRC = string.Empty;

    public class Payload_
    {
        public string Data1 = string.Empty;
        public string Data2 = string.Empty;
        public string Data3 = string.Empty;
        public string Data4 = string.Empty;
        public string Data5 = string.Empty;
    }
}

public class CRC32
{
    static readonly uint[] Crc32Table;
    static CRC32()
    {
        uint polynomial = 0xedb88320;
        Crc32Table = new uint[256];

        for (uint i = 0; i < 256; i++)
        {
            uint crc = i;
            for (uint j = 8; j > 0; j--)
            {
                if ((crc & 1) == 1)
                    crc = (crc >> 1) ^ polynomial;
                else
                    crc >>= 1;
            }
            Crc32Table[i] = crc;
        }
    }
    public static string ComputeCRC32(JsonFormat jsonFormat)
    {
        string input = $"{jsonFormat.Header}{jsonFormat.Date}{jsonFormat.Time}{jsonFormat.Tester}{jsonFormat.Command}";
        input += $"{jsonFormat.Payload.Data1}{jsonFormat.Payload.Data2}{jsonFormat.Payload.Data3}{jsonFormat.Payload.Data4}{jsonFormat.Payload.Data5}";

        uint crc = 0xffffffff;
        byte[] bytes = Encoding.UTF8.GetBytes(input);

        foreach (byte b in bytes)
        {
            byte tableIndex = (byte)((crc & 0xff) ^ b);
            crc = (crc >> 8) ^ Crc32Table[tableIndex];
        }

        crc = ~crc;
        return crc.ToString("X8").ToLower();
    }
}
